# PHP 
======

## What is php?

  

  + PHP stands for HyperText Preprocessor.
  + PHP is an interpreted language, i.e.there is no need for compilation.
  + PHP is a server side scripting language.
  + PHP is faster than other scripting language e.g.asp and jsp.

     * basic example using html view

## What is a PHP File?

  + PHP files can contain text, HTML, CSS, JavaScript, and PHP code
  + PHP code are executed on the server, and the result is returned to the browser as

    plain HTML

  + PHP files have extension ".php"

## What Can PHP Do?

  + PHP can generate dynamic page content
  + PHP can create, open, read, write, delete, and close files on the server
  + PHP can collect form data
  + PHP can send and receive cookies
  + PHP can add, delete, modify data in your database
  + PHP can be used to control user-access
  + PHP can encrypt data..and lot more..

## What Do I Need?

  + Find a web host with PHP and MySQL support
  + Install a web server on your own PC, and then install PHP and MySQL
    - XAMPP Server
    - Sublime Text or Atom
    - Any Modern Web Browser 

## Let's Start!

   - Basic PHP Syntax
   - Semicolon
   - PHP echo

## Comments

   - A comment in PHP code is a line that is not read/executed as part of the program.
   - Its only purpose is to be read by someone who is looking at the code.

     (//, # , /* * /)

## Variable

 
   - Variables are memory location that holds data
   - A variable is a temporary storage that is used to store data temporarily.
   - A variable starts with the $ sign, followed by the name of the variable
   - A variable name must start with a letter or the underscore character
   - A variable name cannot start with a number
   - A variable name can only contain alpha-numeric characters and underscores 

     (A-z, 0-9, and _ )

   - Variable names are case-sensitive ($age and $AGE are two different variables)
   * eg: 

        + echo "I Love $txt!";
        + echo "I love " . $txt . "!";
        + echo $x + $y;
        + echo $string . $int . $float

## PHP Data Types

 	
   - String
   - Integer
   - Float (floating point numbers - also called double)
   - Boolean
   - Array
   - Object
   - NULL

## String

   - strlen(); 
   - str_word_count(); 
   - strrev(); 
   - strpos(); 
   - str_replace(); 

## Operators

   - Arithmetic operators ( + , - , * , / , % , ** (exponentiation x2))
   - Assignment operators ( = )
   - Comparison operators (== , ===, != or <>, !===, >, <, >=, <=)
   - Increment/Decrement operators (++, --)
   - Logical operators (and, or, xor, &&, ||, !)
   - String operators (., .= (concatenation assignment))

<!-- eg: color, number -->

## PHP 5 if... else

``` php
<?php

if(condition) { 
 //code to be executed;
}
```

   - if
   - if-else
   - if-else-if
   - nested if
  

## PHP Switch

``` php
<?php

switch(n)
{
   case 1: break;
   case 2: break;
   default: break;
}
```

## PHP while  Loop

``` php
<?php

while (condition is true) {	
   // code to be executed;
}
```

## PHP do-while Loop

``` php
<?php

do {
   // code to be executed;
} while (condition is true);
```

## PHP for Loop

```php	
<?php

for (init counter; test counter; increment counter) {
   // code to be executed; 
}

``` 

## PHP foreach Loop

```php
<?php

foreach ($array as $value) {
      // code to be executed;
}
```

## PHP Functions

``` php
<?php

function functionName() {
      // code to be executed;
}
```

   - arguments
   - default argument
   - returning values

## PHP Arrays

	

   - An array is a special variable, which can hold more than one value at a time.
   - we can create array using array() or []
   - Indexed arrays - Arrays with a numeric index
   - Associative arrays - Arrays with named keys
   - Multidimensional arrays - Arrays containing one or more arrays
   - Length of an array
   - looping
 

## PHP Global Variables - Superglobals

   
   - always accessible
   - access from any where fun, class, file

   - $GLOBALS
   - $_SERVER
   - $_REQUEST
   - $_POST
   - $_GET
   - $_FILES
   - $_ENV
   - $_COOKIE
   - $_ SESSION

## PHP Form Handling

   - simple html form
   - text field
   - radio button
   - form element
   - empty(); 

## PHP Date

``` php
 date(format, timestamp);
```

   - d - Represents the day of the month (01 to 31)
   - m - Represents a month (01 to 12)
   - Y - Represents a year (in four digits)
   - l (lowercase 'L') - Represents the day of the week

## PHP Include and Required

   include 'filename'; 
   or
   require 'filename'; 

## PHP MySQL Database

